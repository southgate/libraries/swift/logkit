//
//  Test.swift
//  SGTLogging
//
//  Created by David Southgate on 28/02/2025.
//

import Testing

struct Test {

    @Test func <#test function name#>() async throws {
        // Write your test here and use APIs like `#expect(...)` to check expected conditions.
    }

}
