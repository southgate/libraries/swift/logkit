#!/bin/sh
rm -rf build/DerivedData/Build/Products


trap "echo 'Build interrupted. Exiting...'; exit 1" INT

build() {
  SCHEME=$1
  PLATFORM=$2
  header "Building $SCHEME $PLATFORM"
  xcodebuild -scheme "$SCHEME" \
    -project "SampleApp.xcodeproj" \
    -destination "generic/platform=$PLATFORM" \
    -configuration Release \
    -derivedDataPath build/DerivedData \
    -allowProvisioningUpdates \
    -skipPackagePluginValidation \
    -skipMacroValidation \
    CODE_SIGN_IDENTITY="" \
    CODE_SIGNING_REQUIRED=NO \
    build || { echo "Build $SCHEME $PLATFORM Failed" ; exit 1; }
}

header() {
  TEXT=$1
  FULL_TEXT="* $TEXT *"
  LEN=${#FULL_TEXT}
  BORDER=$(printf '*%.0s' $(seq 1 $LEN))
  echo "$BORDER"
  echo "$FULL_TEXT"
  echo "$BORDER"
}

build "SampleApp-iOS-Min" "iOS"
build "SampleApp-iOS-Max" "iOS"
build "SampleApp-iOSUIKit-Min" "iOS"
build "SampleApp-iOSUIKit-Max" "iOS"
build "SampleApp-macOS-Min" "macOS"
build "SampleApp-macOS-Max" "macOS"
build "SampleApp-tvOS-Min" "tvOS"
build "SampleApp-tvOS-Max" "tvOS"
build "SampleApp-visionOS-Min" "visionOS"
build "SampleApp-visionOS-Max" "visionOS"
build "SampleApp-watchOS-Min" "watchOS"
build "SampleApp-watchOS-Max" "watchOS"