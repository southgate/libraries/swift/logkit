import SGTLogging

public class SampleLib {
    public init() {}

    public func go() {
        log.verbose("Verbose")
        log.debug("Debug")
        log.info("Info")
        log.warning("Warning")
        log.error("Error")
    }
}
