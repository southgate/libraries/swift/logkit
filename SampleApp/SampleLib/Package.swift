// swift-tools-version: 6.0
import PackageDescription

let package = Package(
    name: "SampleLib",
    platforms: [.iOS(.v14), .macCatalyst(.v14), .tvOS(.v14), .macOS(.v11), .watchOS(.v7), .visionOS(.v1)],
    products: [
        .library(name: "SampleLib", targets: ["SampleLib"])
    ],
    dependencies: [
        .package(path: "../../")
    ],
    targets: [
        .target(name: "SampleLib", dependencies: ["SGTLogging"])
    ]
)
