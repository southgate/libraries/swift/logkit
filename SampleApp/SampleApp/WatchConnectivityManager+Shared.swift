import SGTLogging

#if os(iOS) || os(watchOS)
extension WatchConnectivityManager {
    static let shared = WatchConnectivityManager(logger: log)
}
#endif
