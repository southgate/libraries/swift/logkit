import SwiftUI

struct ContentView: View {
    var body: some View {
        if #available(iOS 16.0, macOS 13.0, tvOS 16.0, watchOS 9.0, *) {
            NavigationStack {
                HomeView()
            }
        } else {
            NavigationView {
                HomeView()
            }
        }
    }
}
