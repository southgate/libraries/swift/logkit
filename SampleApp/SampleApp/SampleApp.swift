import SGTLogging
import SwiftUI

@main
struct SampleApp: App {
    init() {
        #if os(iOS) || os(watchOS)
        WatchConnectivityManager.shared.activate()
        #endif
    }

    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
