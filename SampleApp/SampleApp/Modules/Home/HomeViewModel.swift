import SampleLib
import SwiftUI

class HomeViewModel {
    func go() {
        log.verbose("Verbose")
        log.debug("Debug")
        log.info("Info")
        log.warning("Warning")
        log.error("Error")
        SampleLib().go()
    }
}
