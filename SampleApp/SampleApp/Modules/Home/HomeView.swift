import SGTLogging
import SwiftUI

struct HomeView: View {
    @State private var viewModel = HomeViewModel()

    var body: some View {
        List {
            NavigationLink("Logs") {
                LogsView(logger: log)
            }
            #if os(watchOS)
            Button("Transfer Logs") {
                WatchConnectivityManager.shared.transferLogs()
            }
            #endif
        }
        .onAppear {
            viewModel.go()
        }
        .navigationTitle("SGTLogging")
    }
}
