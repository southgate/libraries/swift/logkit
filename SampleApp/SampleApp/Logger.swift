import SGTLogging

let log = Logger(destinations: [.os, .rotatingFile(group: group)])

private let group: String? = {
    #if os(watchOS)
    return "watch"
    #else
    return nil
    #endif
}()
