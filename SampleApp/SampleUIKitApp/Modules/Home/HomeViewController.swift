import SGTLogging
import UIKit

class HomeViewController: UITableViewController {
    private let viewModel = HomeViewModel()

    init() {
        super.init(style: .insetGrouped)
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "SGTLogging"
        viewModel.go()
    }

    override func tableView(_: UITableView,
                            numberOfRowsInSection _: Int) -> Int {
        return 1
    }

    override func tableView(_: UITableView,
                            cellForRowAt _: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        cell.textLabel?.text = "Logs"
        cell.accessoryType = .disclosureIndicator
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let logsViewController = LogsViewController(logger: log)
        navigationController?.pushViewController(logsViewController, animated: true)
    }
}
