// swift-tools-version: 6.0
import PackageDescription

let package = Package(
    name: "SGTLogging",
    platforms: [.iOS(.v14), .macCatalyst(.v14), .tvOS(.v14), .macOS(.v11), .watchOS(.v7), .visionOS(.v1)],
    products: [
        .library(name: "SGTLogging", targets: ["SGTLogging"])
    ],
    dependencies: [
        .package(path: "../SGTCore"),
        // .package(url: "git@gitlab.com:southgate/libraries/spm/SGTCore.git", from: "1.0.0"),
        .package(path: "../SGTLogFile"),
        // .package(url: "git@gitlab.com:southgate/libraries/spm/SGTLogFile.git", from: "1.0.0"),
        .package(path: "../SGTRegex"),
        .package(url: "git@gitlab.com:southgate/libraries/spm/SwiftLintPlugins.git", from: "0.58.2"),
        .package(url: "https://github.com/apple/swift-collections.git", from: "1.1.4")
    ],
    targets: [
        .target(name: "SGTLogging",
                dependencies: [
                    "SGTCore",
                    "SGTLogFile",
                    "SGTRegex",
                    .product(name: "Collections", package: "swift-collections")
                ],
                plugins: [
                    .plugin(name: "SwiftLintBuildToolPlugin", package: "SwiftLintPlugins")
                ]),
        .testTarget(name: "SGTLoggingTests", dependencies: ["SGTLogging"])
    ]
)
