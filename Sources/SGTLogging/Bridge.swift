import Combine
import SGTCore

internal final class Bridge: Sendable {
    internal static let shared = Bridge()
    private let subject = SGTMutex(PassthroughSubject<LogContents, Never>())

    internal func doLog(contents: LogContents) {
        subject.value.send(contents)
    }

    internal var publisher: AnyPublisher<LogContents, Never> {
        return subject.value.eraseToAnyPublisher()
    }
}
