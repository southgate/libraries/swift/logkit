import Collections
import Combine
import Foundation
import SGTCore
import SGTLogFile
#if os(watchOS) || os(iOS)
import WatchConnectivity
#endif

internal let log = Logger.library()

public protocol LoggerProtocol: Loggable, Sendable {
    /// Add a LogDestination to the logger
    /// - Parameter destination: The LogDestination to add
    /// - Returns: Whether the destination was successfully added
    func add(destination: any LogDestination) -> Bool

    /// Add an AnyLogDestination to the logger
    /// - Parameter destination: The AnyLogDestination to add
    /// - Returns: Whether the destination was successfully added
    func add(destination: AnyLogDestination) -> Bool

    /// Remove a LogDestination from the logger
    /// - Parameter destination: The LogDestination to remove
    /// - Returns: Whether the destination was successfully removed
    func remove(destination: any LogDestination) -> Bool

    /// Remove an AnyLogDestination from the logger
    /// - Parameter destination: The AnyLogDestination to remove
    /// - Returns: Whether the destination was successfully removed
    func remove(destination: AnyLogDestination) -> Bool

    /// Returns all files managed by the rotating file destination (throws error if no such destination exists)
    /// - Returns: The files
    func files() throws -> [LogFile]

    /// Returns all files managed by the rotating file destination (throws error if no such destination exists) grouped
    /// the group defined in their filename.
    /// - Returns: The files
    func groupedFiles() throws -> [String?: [LogFile]]

    #if os(watchOS)
    /// Transfer all log files managed by the destination using a WCSession
    /// - Parameter session: The WCSession to use to transfer the logs
    func watchConnectivity(transferLogsUsing session: WCSession) throws
    #endif

    #if os(iOS)
    /// Determines whether this file was sent with the intention of being recieved by the logger
    /// - Parameter file: The recieved file
    /// - Returns: Whether the logger should handle this file
    func watchConnectivity(shouldHandleFile file: WCSessionFile) -> Bool

    /// Handle a recieved file using. Only handles file if it was sent by the SGTLogging library
    /// - Parameter file: The recieved file
    func watchConnectivity(didReceive file: WCSessionFile)
    #endif

    /// Moves a file at a given URL to to the folder registered by the primary RotatingFileDestination and triggers a
    /// prune - which **may lead to the moved file being deleted**.
    ///
    /// Throws an error if no RotatingFileDestination has been registered. If multiple have been registered, the first
    /// added will be used.
    /// - Parameter url: URL of file to move
    func importFile(atURL url: URL) throws
}

public final class Logger: LoggerProtocol {
    private static let fileMetadataName = "SGTLogging.File"
    private static let simulatorFileMessageName = "SGTLogging.SimulatorFile"
    private let queue = DispatchQueue(label: "Logger")
    @SGTMutexWrapped private var destinations: OrderedSet<AnyLogDestination> = []
    @SGTMutexWrapped private var bag: Set<AnyCancellable> = []

    public convenience init() {
        self.init(destinations: [])
    }

    public convenience init(destinations: [any LogDestination]) {
        self.init(destinations: destinations.map(AnyLogDestination.init))
    }

    public init(destinations: [AnyLogDestination]) {
        self.destinations.append(contentsOf: destinations)
        setupBridge()
    }

    public static func library() -> any Loggable {
        return Library()
    }

    private func setupBridge() {
        Bridge.shared.publisher
            .sink { [weak self] contents in
                self?.doLog(contents: contents)
            }
            .store(in: &bag)
    }

    @discardableResult
    public func add(destination: any LogDestination) -> Bool {
        return add(destination: AnyLogDestination(destination: destination))
    }

    @discardableResult
    public func add(destination: AnyLogDestination) -> Bool {
        return destinations.append(destination).inserted
    }

    @discardableResult
    public func remove(destination: any LogDestination) -> Bool {
        return remove(destination: AnyLogDestination(destination: destination))
    }

    @discardableResult
    public func remove(destination: AnyLogDestination) -> Bool {
        return destinations.remove(destination) != nil
    }

    public func doLog(contents: LogContents) {
        queue.asyncOrSync(synchronous: contents.synchronous) { [weak self] in
            guard let self else { return }
            let expanded = contents.expanded(date: .injected)
            doLog(contents: expanded)
        }
    }

    public func files() throws -> [LogFile] {
        guard let rotatingFileDestination else {
            throw LoggerError.noRotatingFileDestination
        }
        return try rotatingFileDestination.files()
    }

    public func groupedFiles() throws -> [String?: [LogFile]] {
        guard let rotatingFileDestination else {
            throw LoggerError.noRotatingFileDestination
        }
        var groupedFiles: [String?: [LogFile]] = [:]
        for file in try rotatingFileDestination.files() {
            groupedFiles[file.group, default: []].append(file)
        }
        return groupedFiles
    }

    private func doLog(contents: LogContents.Expanded) {
        for destination in destinations {
            guard contents.excludeDestination != destination else {
                continue
            }
            destination.doLog(contents: contents)
        }
    }

    private final class Library: Loggable {
        func doLog(contents: LogContents) {
            Bridge.shared.doLog(contents: contents)
        }
    }

    #if os(watchOS)
    public func watchConnectivity(transferLogsUsing session: WCSession) throws {
        for file in try files() {
            log.info("Transfering file at \(file.url.absoluteString) to WCSession")
            #if targetEnvironment(simulator)
            session.sendMessage([Self.simulatorFileMessageName: file.url.absoluteString], replyHandler: nil) { error in
                log.error("Could not send log file path", error: error)
            }
            #else
            session.transferFile(file.url, metadata: [
                Self.fileMetadataName: true
            ])
            #endif
        }
    }
    #endif

    #if os(iOS)
    public func watchConnectivity(shouldHandleFile file: WCSessionFile) -> Bool {
        file.metadata?[Self.fileMetadataName] != nil
    }

    public func watchConnectivity(didReceive file: WCSessionFile) {
        guard watchConnectivity(shouldHandleFile: file) else {
            return
        }
        do {
            try importFile(atURL: file.fileURL)
        } catch {
            log.error("Error importing file", error: error)
        }
    }
    #endif

    public func watchConnectivity(shouldHandleMessage message: [String: Any]) -> Bool {
        #if targetEnvironment(simulator)
        message[Self.simulatorFileMessageName] != nil
        #else
        false
        #endif
    }

    public func watchConnectivity(didReceiveMessage message: [String: Any]) {
        #if targetEnvironment(simulator)
        guard watchConnectivity(shouldHandleMessage: message) else {
            return
        }
        guard let path = message[Self.simulatorFileMessageName] as? String,
              let url = URL(string: path) else {
            return
        }
        do {
            let newURL = try FileManager.default.temporaryCopyItem(at: url)
            try importFile(atURL: newURL)
        } catch {
            log.error("Error importing file", error: error)
        }
        #endif
    }

    public func importFile(atURL url: URL) throws {
        guard let rotatingFileDestination else {
            throw LoggerError.noRotatingFileDestination
        }
        try rotatingFileDestination.importFile(atURL: url)
    }

    private var rotatingFileDestination: RotatingFileDestination? {
        for destination in destinations {
            if let rotatingFileDestination = destination.destination as? RotatingFileDestination {
                return rotatingFileDestination
            }
        }
        return nil
    }
}
