import Foundation

public struct LogContents: Sendable {
    public let level: LogLevel
    public let message: @Sendable () -> String?
    public let error: Error?
    public let synchronous: Bool
    public let fileID: String
    public let function: String
    public let line: Int
    public let excludeDestination: AnyLogDestination?

    internal init(level: LogLevel,
                  message: @Sendable @escaping () -> String?,
                  error: Error?,
                  synchronous: Bool,
                  fileID: String,
                  function: String,
                  line: Int,
                  excludeDestination: (any LogDestination)?) {
        self.level = level
        self.message = message
        self.error = error
        self.synchronous = synchronous
        self.fileID = fileID
        self.function = function
        self.line = line
        if let excludeDestination {
            self.excludeDestination = AnyLogDestination(destination: excludeDestination)
        } else {
            self.excludeDestination = nil
        }
    }

    public func expanded(date: Date) -> Expanded {
        let finalMessage = finalMessge(message: message(), error: error)
        let (moduleName, fileName) = expand(fileID: fileID)
        let fileNameWithoutExtension = fileNameWithoutExtension(fileName: fileName)
        return Expanded(date: date,
                        level: level,
                        message: finalMessage,
                        moduleName: moduleName,
                        fileName: fileName,
                        fileNameWithoutExtension: fileNameWithoutExtension,
                        function: function,
                        line: line,
                        synchronous: synchronous,
                        excludeDestination: excludeDestination)
    }

    private func fileNameWithoutExtension(fileName: String) -> String {
        guard let name = fileName.split(separator: ".").first else {
            return fileName
        }
        return String(name)
    }

    public struct Expanded: Sendable {
        public var date: Date
        public let level: LogLevel
        public let message: String?
        public let moduleName: String
        public let fileName: String
        public let fileNameWithoutExtension: String
        public let function: String
        public let line: Int
        public let synchronous: Bool
        public let excludeDestination: AnyLogDestination?
    }

    internal func finalMessge(message: @escaping @autoclosure @Sendable () -> String? = nil,
                              error: Error? = nil) -> String {
        let finalMessage = message()
        if let finalMessage,
           let error {
            return "\(finalMessage) \(error.localizedDescription)"
        }
        if let finalMessage {
            return finalMessage
        }
        if let error {
            return error.localizedDescription
        }
        return ""
    }

    internal func expand(fileID: String) -> (moduleName: String, fileName: String) {
        let split = fileID.split(separator: "/")
        guard split.count == 2, // swiftlint:disable:this no_magic_numbers
              let moduleName = split.first,
              let fileName = split.last else {
            return (moduleName: "", fileName: "")
        }
        return (moduleName: String(moduleName), fileName: String(fileName))
    }
}
