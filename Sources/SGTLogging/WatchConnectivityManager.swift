import SGTCore
#if os(iOS) || os(watchOS)
import WatchConnectivity

/// A default manager for Watch Connectivity if connectivity is only used for logging. If watch connectivity is also
/// used for other purposes, then it is recommended to implement your own solution.
public final class WatchConnectivityManager: NSObject, Sendable {
    private let log: Logger
    @SGTMutexWrapped private var session: WCSession?

    public init(logger: Logger) {
        self.log = logger
        super.init()
        if WCSession.isSupported() {
            self.session = WCSession.default
            session?.delegate = self
        }
    }

    public func activate() {
        session?.activate()
    }

    #if os(watchOS)
    public func transferLogs() {
        guard let session else {
            return
        }
        do {
            try log.watchConnectivity(transferLogsUsing: session)
        } catch {
            log.error("Could not transfer logs", error: error)
        }
    }
    #endif
}

extension WatchConnectivityManager: WCSessionDelegate {
    public func session(_: WCSession,
                        activationDidCompleteWith activationState: WCSessionActivationState,
                        error: Error?) {
        if let error {
            log.error("WatchConnectivity activation completed with error and state '\(activationState.description)'",
                      error: error)
        } else {
            log.info("WatchConnectivity activation completed with state '\(activationState.description)'")
        }
    }

    #if os(watchOS)
    public func session(_: WCSession, didFinish fileTransfer: WCSessionFileTransfer, error: (any Error)?) {
        let url = fileTransfer.file.fileURL.absoluteString
        if let error {
            log.error("WCSession file transfer failed to transfer \(url)", error: error)
        } else {
            log.info("WCSession file transfer did complete \(url)")
        }
    }
    #endif

    public func session(_: WCSession, didReceiveMessage message: [String: Any]) {
        log.watchConnectivity(didReceiveMessage: message)
    }

    #if os(iOS)
    public func session(_: WCSession, didReceive file: WCSessionFile) {
        log.watchConnectivity(didReceive: file)
    }

    public func sessionDidBecomeInactive(_: WCSession) {
        log.info("Watch session did become inactive")
    }

    public func sessionDidDeactivate(_: WCSession) {
        log.info("Watch session did deactivate")
    }
    #endif
}

extension WCSessionActivationState {
    fileprivate var description: String {
        switch self {
        case .notActivated:
            return "notActivated"
        case .inactive:
            return "inactive"
        case .activated:
            return "activated"
        @unknown default:
            return "unknown"
        }
    }
}
#endif
