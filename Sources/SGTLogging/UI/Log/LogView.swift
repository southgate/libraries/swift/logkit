import SGTRegex
import SwiftUI

// swiftlint:disable no_magic_numbers
public struct LogView: View {
    @StateObject private var viewModel: LogViewModel
    @State private var scrollView: ScrollViewProxy?

    public init(name: String, url: URL) {
        self._viewModel = StateObject(wrappedValue: LogViewModel(name: name, url: url))
    }

    public var body: some View {
        ScrollViewReader { scrollView in
            List {
                if viewModel.noLogsFound {
                    Text("No Log Lines Found")
                } else {
                    ForEach(viewModel.lines, id: \.self) { line in
                        Group {
                            #if os(tvOS)
                            Button(line.string) {}
                            #else
                            Text(line.string)
                            #endif
                        }
                        .id(line.number)
                    }
                    .font(.system(.footnote, design: .monospaced))
                }
            }
            .onAppear {
                self.scrollView = scrollView
            }
        }
            .navigationTitle(viewModel.name)
            .toolbar {
                toolbar
            }
            #if os(iOS) || os(visionOS)
            .sheet(isPresented: $viewModel.showingShareSheet) {
                ActivityViewController(activityItems: [viewModel.url])
            }
            #endif
    }

    #if os(iOS) || os(visionOS)
    @ToolbarContentBuilder private var toolbar: some ToolbarContent {
        shareToolbar
        deleteToolbar
        logLevelToolbar
        bottomSpacerToolbar
        logNavigationToolbar
    }
    #elseif os(tvOS) || os(watchOS)
    @ToolbarContentBuilder private var toolbar: some ToolbarContent {
        deleteToolbar
        logNavigationToolbar
    }
    #elseif os(macOS)
    @ToolbarContentBuilder private var toolbar: some ToolbarContent {
        shareToolbar
        deleteToolbar
        logLevelToolbar
        logNavigationToolbar
    }
    #else
    @ToolbarContentBuilder private var toolbar: some ToolbarContent {
        ToolbarItem(placement: .automatic) {
            EmptyView()
        }
    }
    #endif

    @ToolbarContentBuilder private var deleteToolbar: some ToolbarContent {
        ToolbarItem(placement: deleteAndShareToolbarPlacement) {
            Button {
                viewModel.delete()
            } label: {
                Image(systemName: "trash")
                    .accessibilityLabel("Delete current file")
                    .foregroundColor(.red)
            }
        }
    }

    @ToolbarContentBuilder private var shareToolbar: some ToolbarContent {
        ToolbarItem(placement: deleteAndShareToolbarPlacement) {
            Button {
                viewModel.share()
            } label: {
                Image(systemName: "square.and.arrow.up")
                    .accessibilityLabel("Share current file")
            }
        }
    }

    private let deleteAndShareToolbarPlacement: ToolbarItemPlacement = {
        #if os(macOS)
        .automatic
        #else
        if #available(watchOS 10.0, *) {
            return .topBarTrailing
        } else {
            return .automatic
        }
        #endif
    }()

    @ToolbarContentBuilder private var logLevelToolbar: some ToolbarContent {
        ToolbarItem(placement: logLevelAndNavigationToolbarPlacement) {
            LogLevelButton(level: .verbose, enabled: $viewModel.logLevelVerbose)
        }
        ToolbarItem(placement: logLevelAndNavigationToolbarPlacement) {
            LogLevelButton(level: .debug, enabled: $viewModel.logLevelDebug)
        }
        ToolbarItem(placement: logLevelAndNavigationToolbarPlacement) {
            LogLevelButton(level: .info, enabled: $viewModel.logLevelInfo)
        }
        ToolbarItem(placement: logLevelAndNavigationToolbarPlacement) {
            LogLevelButton(level: .warning, enabled: $viewModel.logLevelWarning)
        }
        ToolbarItem(placement: logLevelAndNavigationToolbarPlacement) {
            LogLevelButton(level: .error, enabled: $viewModel.logLevelError)
        }
        ToolbarItem(placement: logLevelAndNavigationToolbarPlacement) {
            LogLevelButton(level: .fatal, enabled: $viewModel.logLevelFatal)
        }
    }

    @ToolbarContentBuilder private var bottomSpacerToolbar: some ToolbarContent {
        ToolbarItem(placement: logLevelAndNavigationToolbarPlacement) {
            Spacer()
        }
    }

    @ToolbarContentBuilder private var logNavigationToolbar: some ToolbarContent {
        ToolbarItem(placement: logLevelAndNavigationToolbarPlacement) {
            Button(action: {
                withAnimation {
                    scrollView?.scrollTo(viewModel.lines.first?.number)
                }
            }, label: {
                Image(systemName: "arrow.up")
                    .accessibilityLabel("Scroll to top")
            })
        }
        ToolbarItem(placement: logLevelAndNavigationToolbarPlacement) {
            Button(action: {
                withAnimation {
                    scrollView?.scrollTo(viewModel.lines.last?.number)
                }
            }, label: {
                Image(systemName: "arrow.down")
                    .accessibilityLabel("Scroll to bottom")
            })
        }
    }

    private let logLevelAndNavigationToolbarPlacement: ToolbarItemPlacement = {
        #if os(macOS)
        .automatic
        #else
        if #available(watchOS 10.0, tvOS 18.0, *) {
            return .bottomBar
        } else {
            return .automatic
        }
        #endif
    }()
}

private struct LogLevelButton: View {
    let level: LogLevel
    @Binding var enabled: Bool

    var body: some View {
        Button(action: {
            enabled.toggle()
        }, label: {
            VStack(spacing: 5) {
                Text(level.emoji)
                Circle()
                    .frame(width: 5, height: 5, alignment: .center)
                    .foregroundColor(enabled ? Color.blue : Color.gray)
            }
        })
    }
}
// swiftlint:enable no_magic_numbers
