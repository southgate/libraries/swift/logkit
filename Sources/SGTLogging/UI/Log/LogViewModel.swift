import Combine
import Foundation
import SGTCore
import SGTRegex

@MainActor
internal final class LogViewModel: ObservableObject {
    @SGTMutexWrapped private var bag: Set<AnyCancellable> = []
    private let regex: Regex?
    internal let name: String
    internal let url: URL

    internal init(name: String, url: URL) {
        self.name = name
        self.url = url
        self.regex = Self.setupRegex()
        self.allLines = []
        self.lines = []

        self.loadAllLines()

        $logLevelVerbose
            .sink { [weak self] newValue in
                self?.filterLines(newValueLevel: .verbose, newValue: newValue)
            }
            .store(in: &bag)

        $logLevelDebug
            .sink { [weak self] newValue in
                self?.filterLines(newValueLevel: .debug, newValue: newValue)
            }
            .store(in: &bag)

        $logLevelInfo
            .sink { [weak self] newValue in
                self?.filterLines(newValueLevel: .info, newValue: newValue)
            }
            .store(in: &bag)

        $logLevelWarning
            .sink { [weak self] newValue in
                self?.filterLines(newValueLevel: .warning, newValue: newValue)
            }
            .store(in: &bag)

        $logLevelError
            .sink { [weak self] newValue in
                self?.filterLines(newValueLevel: .error, newValue: newValue)
            }
            .store(in: &bag)

        $logLevelFatal
            .sink { [weak self] newValue in
                self?.filterLines(newValueLevel: .fatal, newValue: newValue)
            }
            .store(in: &bag)
    }

    private func filterLines(newValueLevel: LogLevel, newValue: Bool) {
        let newLines = self.allLines.filter { [weak self] line in
            guard let self,
                  let level = line.level else {
                return true
            }
            if level == newValueLevel {
                return newValue
            }
            switch level {
            case .verbose:  return self.logLevelVerbose
            case .debug:    return self.logLevelDebug
            case .info:     return self.logLevelInfo
            case .warning:  return self.logLevelWarning
            case .error:    return self.logLevelError
            case .fatal:    return self.logLevelFatal
            }
        }
        self.lines = newLines
    }

    private var allLines: [Line]

    @Published internal var lines: [Line]
    @Published internal var showingShareSheet: Bool = false
    @Published internal var gotoLogs: Bool = false
    @Published internal var logLevelVerbose: Bool = true
    @Published internal var logLevelDebug: Bool = true
    @Published internal var logLevelLoggerInternal: Bool = true
    @Published internal var logLevelInfo: Bool = true
    @Published internal var logLevelWarning: Bool = true
    @Published internal var logLevelError: Bool = true
    @Published internal var logLevelFatal: Bool = true

    internal var noLogsFound: Bool {
        return self.lines.isEmpty
    }

    internal func share() {
        showingShareSheet = true
    }

    internal func delete() {
        do {
            try FileManager.default.removeItem(at: url)
            let line = Line("File Deleted", number: 0, regex: nil)
            lines = [line]
            allLines = [line]
        } catch {
            log.error(error: error)
        }
    }

    private func loadAllLines() {
        Task { [weak self] in
            guard let self else { return }
            do {
                let fileContents = try String(contentsOf: url, encoding: .utf8)
                var lineNumber = 0
                let allLines = fileContents.components(separatedBy: .newlines).compactMap { line -> Line? in
                    guard !line.isEmpty else { return nil }
                    lineNumber += 1
                    return Line(line, number: lineNumber, regex: self.regex)
                }
                self.allLines = allLines
                DispatchQueue.main.async { [weak self] in
                    self?.lines = allLines
                }
            } catch {
                log.error(error: error)
            }
        }
    }

    private static func setupRegex() -> Regex? {
        do {
            let pattern = "^(?<timestamp>[^ ]* [^ ]*) (?<level>◽️|◾️|🪵|🔷|🔶|❌|💀) (?<file>[^.]*)\\." +
                          "(?<function>[^(]*\\([^)]*\\)):(?<line>[0-9]+)( - (?<message>.*)){0,1}$"
            return try Regex(pattern: pattern)
        } catch {
            log.error(error: error)
            return nil
        }
    }

    internal struct Line: Hashable {
        internal let string: String
        internal let number: Int
        internal let regexResult: RegexResult?

        internal init(_ string: String, number: Int, regex: Regex?) {
            self.string = string
            self.number = number
            self.regexResult = regex?.firstMatch(in: string)
        }

        internal var level: LogLevel? {
            guard let regexResult,
                  let emoji = regexResult.group(withName: "level") else {
                return nil
            }
            return LogLevel(emoji: emoji)
        }

        internal static func == (lhs: Self, rhs: Self) -> Bool {
            return lhs.number == rhs.number
        }

        internal func hash(into hasher: inout Hasher) {
            self.number.hash(into: &hasher)
        }
    }
}
