import SwiftUI

public struct LogsView: View {
    @StateObject private var viewModel: LogsViewModel

    public init(logger: Logger) {
        self._viewModel = StateObject(wrappedValue: LogsViewModel(logger: logger))
    }

    public var body: some View {
        List {
            if viewModel.noLogsFound {
                Text("No Log Files Found")
            } else {
                ForEach(viewModel.sections) { section in
                    LogsSection(section: section)
                }
            }
        }
        .onAppear {
            do {
                try viewModel.load()
            } catch {
                log.error(error: error)
            }
        }
        .customRefreshable {
            do {
                try await viewModel.load()
            } catch {
                log.error(error: error)
            }
        }
        .navigationTitle("Logs")
    }
}

private struct LogsSection: View {
    fileprivate let section: LogsViewModel.Section

    fileprivate var body: some View {
        Section(header: LogsSectionHeader(section: section)) {
            ForEach(section.files) { file in
                LogsFileRow(file: file)
            }
        }
    }
}

private struct LogsFileRow: View {
    fileprivate let file: LogsViewModel.File

    fileprivate var body: some View {
        NavigationLink(file.name) {
            LogView(name: file.name, url: file.url)
        }
    }
}

private struct LogsSectionHeader: View {
    fileprivate let section: LogsViewModel.Section

    fileprivate var body: some View {
        if let title = section.title {
            Text(title)
        } else {
            EmptyView()
        }
    }
}
