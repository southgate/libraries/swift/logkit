import SGTLogFile
import SwiftUI

@MainActor
internal class LogsViewModel: ObservableObject {
    private let logger: LoggerProtocol
    @Published internal var sections: [Section] = []

    internal init(logger: LoggerProtocol) {
        self.logger = logger
    }

    internal func load() throws {
        let groupedFiles = try logger.groupedFiles()
        let keys = groupedFiles.keys.sorted { lhs, rhs in
            guard let lhs,
                  let rhs else {
                return true
            }
            return lhs < rhs
        }
        var sections: [Section] = []
        var id = 0
        for key in keys {
            guard let logFiles = groupedFiles[key],
                  logFiles.isNotEmpty else {
                continue
            }
            let files = transformFiles(logFiles)
            let title = title(keys: keys, key: key)
            sections.append(Section(id: id, title: title, files: files))
            id += 1
        }
        self.sections = sections
    }

    internal var noLogsFound: Bool {
        sections.isEmpty
    }

    private func transformFiles(_ files: [LogFile]) -> [File] {
        return files.enumerated().map { id, logFile in
            File(id: id, name: logFile.name, url: logFile.url)
        }
    }

    private func title(keys: [String?], key: String?) -> String? {
        if keys.count > 1 {
            return key ?? "This Device"
        } else {
            return nil
        }
    }

    internal struct Section: Identifiable {
        internal let id: Int
        internal let title: String?
        internal let files: [File]
    }

    internal struct File: Identifiable {
        internal let id: Int
        internal let name: String
        internal let url: URL
    }
}

extension Sequence where Element == String? {
    fileprivate func sorted() -> [Optional<String>] {
        return self.sorted { lhs, rhs in
            guard let lhs,
                  let rhs else {
                return false
            }
            return lhs < rhs
        }
    }
}
