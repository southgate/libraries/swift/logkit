#if os(iOS)
import SwiftUI
import UIKit

public class LogsViewController: UIHostingController<LogsView> {
    public init(logger: Logger) {
        super.init(rootView: LogsView(logger: logger))
    }

    @available(*, unavailable)
    internal required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override public func viewDidLoad() {
        super.viewDidLoad()
        title = "Logs"
        navigationController?.isToolbarHidden = false
    }
}
#endif
