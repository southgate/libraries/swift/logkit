import SwiftUI

extension View {
    nonisolated internal func customRefreshable(action: @escaping @Sendable () async -> Void) -> some View {
        #if os(iOS) || os(visionOS)
        if #available(iOS 15.0, *) {
            return self.refreshable(action: action)
        } else {
            return self.toolbarRefreshable(action: action)
        }
        #else
        return self.toolbarRefreshable(action: action)
        #endif
    }

    nonisolated private func toolbarRefreshable(action: @escaping @Sendable () async -> Void) -> some View {
        return self.toolbar {
            #if os(iOS)
            let placement: ToolbarItemPlacement = .topBarTrailing
            #else
            let placement: ToolbarItemPlacement = .automatic
            #endif
            ToolbarItem(placement: placement) {
                Button {
                    Task {
                        await action()
                    }
                } label: {
                    Image(systemName: "arrow.clockwise")
                }
            }
        }
    }
}
