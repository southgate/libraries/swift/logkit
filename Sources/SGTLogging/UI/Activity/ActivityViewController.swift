#if os(iOS) || os(visionOS)
import SwiftUI
import UIKit

internal struct ActivityViewController: UIViewControllerRepresentable {
    internal let activityItems: [URL]
    internal let applicationActivities: [UIActivity]?

    internal init(activityItems: [URL], applicationActivities: [UIActivity]? = nil) {
        self.activityItems = activityItems
        self.applicationActivities = applicationActivities
    }

    internal func makeUIViewController(
        context _: UIViewControllerRepresentableContext<Self>
    ) -> UIActivityViewController {
        return UIActivityViewController(activityItems: activityItems, applicationActivities: applicationActivities)
    }

    internal func updateUIViewController(
        _: UIActivityViewController,
        context _: UIViewControllerRepresentableContext<Self>
    ) {}
}
#endif
