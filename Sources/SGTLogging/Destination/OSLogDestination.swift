import Foundation
import os.log
import SGTLogFile

/// A log destination that outputs to OSLog
public final class OSLogDestination: LogDestination {
    private let formatter: LogFormatter

    public init(formatter: LogFormatter? = nil) {
        let formatter = formatter ?? DefaultLogFormatter()
        self.formatter = formatter
    }

    public func doLog(contents: LogContents.Expanded) {
        let osLog = osLog(contents: contents)
        let message = formatter.format(contents: contents)
        os_log("%@", log: osLog, type: osLogType(contents.level), message)
    }

    private func osLog(contents: LogContents.Expanded) -> OSLog {
        return OSLog(subsystem: subsystem(contents: contents), category: contents.fileName)
    }

    private func osLogType(_ level: LogLevel) -> OSLogType {
        switch level {
        case .verbose:
            return .default
        case .debug:
            return .debug
        case .info:
            return .info
        case .warning:
            return .error
        case .error:
            return .fault
        case .fatal:
            return .fault
        }
    }

    private func subsystem(contents: LogContents.Expanded) -> String {
        if let bundleID = Bundle.main.bundleIdentifier {
            return "\(bundleID).\(contents.moduleName)"
        } else {
            return contents.moduleName
        }
    }
}
