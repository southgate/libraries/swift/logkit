import Foundation
import SGTLogFile

/// A log destination that prints logs to the console
public final class ConsoleDestination: LogDestination {
    private let formatter: LogFormatter

    /// - Parameter formatter: The log formatter to use
    public init(formatter: LogFormatter? = nil) {
        let formatter = formatter ?? DefaultLogFormatter()
        self.formatter = formatter
    }

    public func doLog(contents: LogContents.Expanded) {
        print(formatter.format(contents: contents)) // swiftlint:disable:this avoid_print
    }
}
