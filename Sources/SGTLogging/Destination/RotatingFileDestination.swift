import Foundation
import SGTCore
import SGTLogFile

/// A log destination that outputs files in a directory. Using a distinct file per day where old files are
/// automatically cleaned up
public final class RotatingFileDestination: LogDestination {
    public static let defaultDirectory: URL = .cachesDirectory.appendingPathComponent("log")
    public static let defaultDaysToKeep = 7
    public let directory: URL
    public let group: String?
    private let logFileDirectory: LogFileDirectory
    private let formatter: LogFormatter

    public init(directory: URL? = nil, group: String? = nil, daysToKeep: Int? = nil, formatter: LogFormatter? = nil) {
        let directory = directory ?? Self.defaultDirectory
        let daysToKeep = daysToKeep ?? Self.defaultDaysToKeep
        let formatter = formatter ?? DefaultLogFormatter()
        self.directory = directory
        self.group = group
        self.formatter = formatter
        self.logFileDirectory = LogFileDirectory(directory: directory,
                                                 fileNamingStrategy: .date,
                                                 fileExtension: "log",
                                                 pruneStrategy: .days(daysToKeep: daysToKeep),
                                                 overwrite: false)
        prune()
    }

    public func doLog(contents: LogContents.Expanded) {
        let line = formatter.format(contents: contents)
        do {
            try logFileDirectory.write(line: line, date: contents.date, group: group)
        } catch {
            log.error("Could not write to log", error: error)
        }
    }

    public func files() throws -> [LogFile] {
        return try logFileDirectory.files()
    }

    public func importFile(atURL url: URL) throws {
        try logFileDirectory.importFile(atURL: url)
    }

    public func prune() {
        do {
            try logFileDirectory.prune()
        } catch {
            log.error("Could prune logs", error: error)
        }
    }
}
