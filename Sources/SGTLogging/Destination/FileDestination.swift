import Foundation
import SGTLogFile

/// A log destination that logs to a single file. **WARNING: This file is never cleaned up**
public final class FileDestination: LogDestination {
    private let writer: LogFileWriter?
    private let formatter: LogFormatter

    /// - Parameters:
    ///   - fileURL: The URL of the file to use
    ///   - formatter: The log formatter to use
    public init(fileURL: URL, formatter: LogFormatter? = nil) {
        let formatter = formatter ?? DefaultLogFormatter()
        self.formatter = formatter
        do {
            self.writer = try LogFileWriter(fileURL: fileURL)
        } catch {
            self.writer = nil
            log.error("Error opening log file", error: error, excludeDestination: self)
        }
    }

    public func doLog(contents: LogContents.Expanded) {
        guard let writer else {
            return
        }
        let line = formatter.format(contents: contents)
        do {
            try writer.write(line: line)
        } catch {
            log.error("Could not write line to log file", error: error, excludeDestination: self)
        }
    }
}
