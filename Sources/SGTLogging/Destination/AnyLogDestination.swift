import Foundation
import SGTCore
import SGTLogFile

/// Type erased wrapper around a LogDestination
public struct AnyLogDestination: Equatable, Hashable, Sendable {
    public static let console = Self(destination: ConsoleDestination())
    public static let os = Self(destination: OSLogDestination()) // swiftlint:disable:this identifier_name

    public static func file(at fileURL: URL) -> Self {
        Self(destination: FileDestination(fileURL: fileURL))
    }

    public static let rotatingFile = Self(destination: RotatingFileDestination())

    public static func rotatingFile(directory: URL? = nil, group: String? = nil) -> Self {
        Self(destination: RotatingFileDestination(directory: directory, group: group))
    }

    public let destination: any LogDestination

    public init(destination: any LogDestination) {
        self.destination = destination
    }
}

extension AnyLogDestination: LogDestination {
    public func doLog(contents: LogContents.Expanded) {
        destination.doLog(contents: contents)
    }
}

extension AnyLogDestination {
    /// Returns a Boolean value indicating whether two values are equal.
    ///
    /// Equality is the inverse of inequality. For any values `a` and `b`,
    /// `a == b` implies that `a != b` is `false`.
    ///
    /// - Parameters:
    ///   - lhs: A value to compare.
    ///   - rhs: Another value to compare.
    public static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.destination.hashValue == rhs.destination.hashValue
    }

    /// Hashes the essential components of this value by feeding them into the
    /// given hasher.
    ///
    /// Implement this method to conform to the `Hashable` protocol. The
    /// components used for hashing must be the same as the components compared
    /// in your type's `==` operator implementation. Call `hasher.combine(_:)`
    /// with each of these components.
    ///
    /// - Important: In your implementation of `hash(into:)`,
    ///   don't call `finalize()` on the `hasher` instance provided,
    ///   or replace it with a different instance.
    ///   Doing so may become a compile-time error in the future.
    ///
    /// - Parameter hasher: The hasher to use when combining the components
    ///   of this instance.
    public func hash(into hasher: inout Hasher) {
        destination.hash(into: &hasher)
    }
}

extension Set where Element == AnyLogDestination {
    @discardableResult
    @inlinable
    internal mutating func insert(
        _ newMembers: [any LogDestination]
    ) -> InsertMembersResult {
        return self.insert(newMembers.map(AnyLogDestination.init))
    }
}
