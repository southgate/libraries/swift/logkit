import Foundation

/// The default log formatter
public final class DefaultLogFormatter: LogFormatter {
    private let dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
        return dateFormatter
    }()

    public init() {
        // No content
    }

    public func format(contents: LogContents.Expanded) -> String {
        let date = dateFormatter.string(from: contents.date)
        let emoji = contents.level.emoji
        let message = messageWithSeparator(contents: contents)
        return "\(date) \(emoji) [\(contents.moduleName)] \(contents.fileNameWithoutExtension).\(contents.function):" +
              "\(contents.line) \(message)"
    }

    private func messageWithSeparator(contents: LogContents.Expanded) -> String {
        guard let message = contents.message else {
            return ""
        }
        return "- \(message)"
    }
}
