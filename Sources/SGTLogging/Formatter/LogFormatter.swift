import Foundation

/// Formater that produces string for log
public protocol LogFormatter: Sendable {
    /// Produce a formatted string for a log
    /// - Parameter contents: The log contents
    /// - Returns: The string
    func format(contents: LogContents.Expanded) -> String
}
