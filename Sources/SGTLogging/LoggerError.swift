//
//  LoggerError.swift
//  SGTLogging
//
//  Created by David Southgate on 28/02/2025.
//

public enum LoggerError: Error {
    case noRotatingFileDestination
}
