import Foundation

extension DispatchQueue {
    internal func asyncOrSync(synchronous: Bool, execute work: @escaping @Sendable @convention(block) () -> Void) {
        if synchronous {
            self.sync(execute: work)
        } else {
            self.async(execute: work)
        }
    }
}
