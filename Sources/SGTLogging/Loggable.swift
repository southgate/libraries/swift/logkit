public protocol Loggable: Sendable {
    func doLog(contents: LogContents)
}

extension Loggable {
    private func log(level: LogLevel,
                     message: @escaping @autoclosure @Sendable () -> String?,
                     error: Error? = nil,
                     synchronous: Bool = false,
                     fileID: String = #fileID,
                     function: String = #function,
                     line: Int = #line,
                     excludeDestination: (any LogDestination)? = nil) {
        self.doLog(contents: LogContents(level: level,
                                         message: message,
                                         error: error,
                                         synchronous: synchronous,
                                         fileID: fileID,
                                         function: function,
                                         line: line,
                                         excludeDestination: excludeDestination))
    }

    /// Logs with the verbose log level.
    /// - Parameters:
    ///   - message: The message to log
    ///   - fileID: The fileID. Defaults to `#fileID`
    ///   - function: The function. Defaults to `#function`
    ///   - line: The line. Defaults to `#line`
    public func verbose(_ message: @escaping @autoclosure @Sendable () -> String,
                        _ fileID: String = #fileID,
                        _ function: String = #function,
                        line: Int = #line) {
        log(level: .verbose,
            message: message(),
            fileID: fileID,
            function: function,
            line: line)
    }

    /// Logs with the verbose log level.
    /// - Parameters:
    ///   - string: The message to log. As a raw string if using autoclosure is not ideal.
    ///   - fileID: The fileID. Defaults to `#fileID`
    ///   - function: The function. Defaults to `#function`
    ///   - line: The line. Defaults to `#line`
    public func verbose(string: String,
                        _ fileID: String = #fileID,
                        _ function: String = #function,
                        line: Int = #line) {
        log(level: .verbose,
            message: string,
            fileID: fileID,
            function: function,
            line: line)
    }

    /// Logs with the debug log level.
    /// - Parameters:
    ///   - message: The message to log
    ///   - fileID: The fileID. Defaults to `#fileID`
    ///   - function: The function. Defaults to `#function`
    ///   - line: The line. Defaults to `#line`
    public func debug(_ message: @escaping @autoclosure @Sendable () -> String,
                      _ fileID: String = #fileID,
                      _ function: String = #function,
                      line: Int = #line) {
        log(level: .debug,
            message: message(),
            fileID: fileID,
            function: function,
            line: line)
    }

    /// Logs with the debug log level.
    /// - Parameters:
    ///   - string: The message to log. As a raw string if using autoclosure is not ideal.
    ///   - fileID: The fileID. Defaults to `#fileID`
    ///   - function: The function. Defaults to `#function`
    ///   - line: The line. Defaults to `#line`
    public func debug(string: String,
                      _ fileID: String = #fileID,
                      _ function: String = #function,
                      line: Int = #line) {
        log(level: .debug,
            message: string,
            fileID: fileID,
            function: function,
            line: line)
    }

    /// Logs with the info log level.
    /// - Parameters:
    ///   - message: The message to log
    ///   - fileID: The fileID. Defaults to `#fileID`
    ///   - function: The function. Defaults to `#function`
    ///   - line: The line. Defaults to `#line`
    public func info(_ message: @escaping @autoclosure @Sendable () -> String,
                     _ fileID: String = #fileID,
                     _ function: String = #function,
                     line: Int = #line) {
        log(level: .info,
            message: message(),
            fileID: fileID,
            function: function,
            line: line)
    }

    /// Logs with the info log level.
    /// - Parameters:
    ///   - string: The message to log. As a raw string if using autoclosure is not ideal.
    ///   - fileID: The fileID. Defaults to `#fileID`
    ///   - function: The function. Defaults to `#function`
    ///   - line: The line. Defaults to `#line`
    public func info(string: String,
                     _ fileID: String = #fileID,
                     _ function: String = #function,
                     line: Int = #line) {
        log(level: .info,
            message: string,
            fileID: fileID,
            function: function,
            line: line)
    }

    /// Logs with the warning log level.
    /// - Parameters:
    ///   - message: The message to log
    ///   - fileID: The fileID. Defaults to `#fileID`
    ///   - function: The function. Defaults to `#function`
    ///   - line: The line. Defaults to `#line`
    public func warning(_ message: @escaping @autoclosure @Sendable () -> String,
                        _ fileID: String = #fileID,
                        _ function: String = #function,
                        line: Int = #line) {
        log(level: .warning,
            message: message(),
            fileID: fileID,
            function: function,
            line: line)
    }

    /// Logs with the warning log level.
    /// - Parameters:
    ///   - string: The message to log. As a raw string if using autoclosure is not ideal.
    ///   - fileID: The fileID. Defaults to `#fileID`
    ///   - function: The function. Defaults to `#function`
    ///   - line: The line. Defaults to `#line`
    public func warning(string: String,
                        _ fileID: String = #fileID,
                        _ function: String = #function,
                        line: Int = #line) {
        log(level: .warning,
            message: string,
            fileID: fileID,
            function: function,
            line: line)
    }

    /// Logs with the warning log level.
    /// - Parameters:
    ///   - message: The message to log
    ///   - error: The error to log
    ///   - fileID: The fileID. Defaults to `#fileID`
    ///   - function: The function. Defaults to `#function`
    ///   - line: The line. Defaults to `#line`
    public func warning(_ message: @escaping @autoclosure @Sendable () -> String,
                        error: Error,
                        _ fileID: String = #fileID,
                        _ function: String = #function,
                        line: Int = #line) {
        log(level: .warning,
            message: message(),
            error: error,
            fileID: fileID,
            function: function,
            line: line)
    }

    /// Logs with the warning log level.
    /// - Parameters:
    ///   - string: The message to log. As a raw string if using autoclosure is not ideal.
    ///   - error: The error to log
    ///   - fileID: The fileID. Defaults to `#fileID`
    ///   - function: The function. Defaults to `#function`
    ///   - line: The line. Defaults to `#line`
    public func warning(string: String,
                        error: Error,
                        _ fileID: String = #fileID,
                        _ function: String = #function,
                        line: Int = #line) {
        log(level: .warning,
            message: string,
            error: error,
            fileID: fileID,
            function: function,
            line: line)
    }

    /// Logs with the warning error level.
    /// - Parameters:
    ///   - error: The error to log
    ///   - fileID: The fileID. Defaults to `#fileID`
    ///   - function: The function. Defaults to `#function`
    ///   - line: The line. Defaults to `#line`
    public func warning(error: Error,
                        _ fileID: String = #fileID,
                        _ function: String = #function,
                        line: Int = #line) {
        log(level: .warning,
            message: nil,
            error: error,
            fileID: fileID,
            function: function,
            line: line)
    }

    /// Logs with the error log level.
    /// - Parameters:
    ///   - message: The message to log
    ///   - fileID: The fileID. Defaults to `#fileID`
    ///   - function: The function. Defaults to `#function`
    ///   - line: The line. Defaults to `#line`
    public func error(_ message: @escaping @autoclosure @Sendable () -> String,
                      _ fileID: String = #fileID,
                      _ function: String = #function,
                      line: Int = #line) {
        log(level: .error,
            message: message(),
            fileID: fileID,
            function: function,
            line: line)
    }

    /// Logs with the error log level.
    /// - Parameters:
    ///   - string: The message to log. As a raw string if using autoclosure is not ideal.
    ///   - fileID: The fileID. Defaults to `#fileID`
    ///   - function: The function. Defaults to `#function`
    ///   - line: The line. Defaults to `#line`
    public func error(string: String,
                      _ fileID: String = #fileID,
                      _ function: String = #function,
                      line: Int = #line) {
        log(level: .error,
            message: string,
            fileID: fileID,
            function: function,
            line: line)
    }

    /// Logs with the error log level.
    /// - Parameters:
    ///   - message: The message to log
    ///   - error: The error to log
    ///   - fileID: The fileID. Defaults to `#fileID`
    ///   - function: The function. Defaults to `#function`
    ///   - line: The line. Defaults to `#line`
    public func error(_ message: @escaping @autoclosure @Sendable () -> String,
                      error: Error,
                      _ fileID: String = #fileID,
                      _ function: String = #function,
                      line: Int = #line) {
        log(level: .error,
            message: message(),
            error: error,
            fileID: fileID,
            function: function,
            line: line)
    }

    /// Logs with the error log level.
    /// - Parameters:
    ///   - string: The message to log. As a raw string if using autoclosure is not ideal.
    ///   - error: The error to log
    ///   - fileID: The fileID. Defaults to `#fileID`
    ///   - function: The function. Defaults to `#function`
    ///   - line: The line. Defaults to `#line`
    public func error(string: String,
                      error: Error,
                      _ fileID: String = #fileID,
                      _ function: String = #function,
                      line: Int = #line) {
        log(level: .error,
            message: string,
            error: error,
            fileID: fileID,
            function: function,
            line: line)
    }

    /// Logs with the error log level.
    /// - Parameters:
    ///   - error: The error to log
    ///   - fileID: The fileID. Defaults to `#fileID`
    ///   - function: The function. Defaults to `#function`
    ///   - line: The line. Defaults to `#line`
    public func error(error: Error,
                      _ fileID: String = #fileID,
                      _ function: String = #function,
                      line: Int = #line) {
        log(level: .error,
            message: nil,
            error: error,
            fileID: fileID,
            function: function,
            line: line)
    }

    internal func error(_ message: @escaping @autoclosure @Sendable () -> String,
                        _ fileID: String = #fileID,
                        _ function: String = #function,
                        line: Int = #line,
                        excludeDestination: any LogDestination) {
        log(level: .error,
            message: message(),
            fileID: fileID,
            function: function,
            line: line,
            excludeDestination: excludeDestination)
    }

    internal func error(string: String,
                        _ fileID: String = #fileID,
                        _ function: String = #function,
                        line: Int = #line,
                        excludeDestination: any LogDestination) {
        log(level: .error,
            message: string,
            fileID: fileID,
            function: function,
            line: line,
            excludeDestination: excludeDestination)
    }

    internal func error(_ message: @escaping @autoclosure @Sendable () -> String,
                        error: Error,
                        _ fileID: String = #fileID,
                        _ function: String = #function,
                        line: Int = #line,
                        excludeDestination: any LogDestination) {
        log(level: .error,
            message: message(),
            error: error,
            fileID: fileID,
            function: function,
            line: line,
            excludeDestination: excludeDestination)
    }

    internal func error(string: String,
                        error: Error,
                        _ fileID: String = #fileID,
                        _ function: String = #function,
                        line: Int = #line,
                        excludeDestination: any LogDestination) {
        log(level: .error,
            message: string,
            error: error,
            fileID: fileID,
            function: function,
            line: line,
            excludeDestination: excludeDestination)
    }

    internal func error(error: Error,
                        _ fileID: String = #fileID,
                        _ function: String = #function,
                        line: Int = #line,
                        excludeDestination: any LogDestination) {
        log(level: .error,
            message: nil,
            error: error,
            fileID: fileID,
            function: function,
            line: line,
            excludeDestination: excludeDestination)
    }

    /// Logs with the fatal log level and halts execution.
    /// - Parameters:
    ///   - message: The message to log
    ///   - fileID: The fileID. Defaults to `#fileID`
    ///   - function: The function. Defaults to `#function`
    ///   - line: The line. Defaults to `#line`
    public func fatal(_ message: @escaping @autoclosure @Sendable () -> String,
                      _ fileID: String = #fileID,
                      _ function: String = #function,
                      line: Int = #line) -> Never {
        log(level: .fatal,
            message: message(),
            synchronous: true,
            fileID: fileID,
            function: function,
            line: line)
        fatalError(finalMessge(message: message()))
    }

    /// Logs with the fatal log level and halts execution.
    /// - Parameters:
    ///   - string: The message to log. As a raw string if using autoclosure is not ideal.
    ///   - fileID: The fileID. Defaults to `#fileID`
    ///   - function: The function. Defaults to `#function`
    ///   - line: The line. Defaults to `#line`
    public func fatal(string: String,
                      _ fileID: String = #fileID,
                      _ function: String = #function,
                      line: Int = #line) -> Never {
        log(level: .fatal,
            message: string,
            synchronous: true,
            fileID: fileID,
            function: function,
            line: line)
        fatalError(finalMessge(message: string))
    }

    /// Logs with the fatal log level and halts execution.
    /// - Parameters:
    ///   - message: The message to log
    ///   - error: The error to log
    ///   - fileID: The fileID. Defaults to `#fileID`
    ///   - function: The function. Defaults to `#function`
    ///   - line: The line. Defaults to `#line`
    public func fatal(_ message: @escaping @autoclosure @Sendable () -> String,
                      error: Error,
                      _ fileID: String = #fileID,
                      _ function: String = #function,
                      line: Int = #line) -> Never {
        log(level: .fatal,
            message: message(),
            error: error,
            synchronous: true,
            fileID: fileID,
            function: function,
            line: line)
        fatalError(finalMessge(message: message(), error: error))
    }

    /// Logs with the fatal log level and halts execution.
    /// - Parameters:
    ///   - string: The message to log. As a raw string if using autoclosure is not ideal.
    ///   - error: The error to log
    ///   - fileID: The fileID. Defaults to `#fileID`
    ///   - function: The function. Defaults to `#function`
    ///   - line: The line. Defaults to `#line`
    public func fatal(string: String,
                      error: Error,
                      _ fileID: String = #fileID,
                      _ function: String = #function,
                      line: Int = #line) -> Never {
        log(level: .fatal,
            message: string,
            error: error,
            synchronous: true,
            fileID: fileID,
            function: function,
            line: line)
        fatalError(finalMessge(message: string, error: error))
    }

    /// Logs with the fatal log level and halts execution.
    /// - Parameters:
    ///   - error: The error to log
    ///   - fileID: The fileID. Defaults to `#fileID`
    ///   - function: The function. Defaults to `#function`
    ///   - line: The line. Defaults to `#line`
    public func fatal(error: Error,
                      _ fileID: String = #fileID,
                      _ function: String = #function,
                      line: Int = #line) -> Never {
        log(level: .fatal,
            message: nil,
            error: error,
            synchronous: true,
            fileID: fileID,
            function: function,
            line: line)
        fatalError(finalMessge(error: error))
    }

    internal func fatal(_ message: @escaping @autoclosure @Sendable () -> String,
                        _ fileID: String = #fileID,
                        _ function: String = #function,
                        line: Int = #line,
                        excludeDestination: any LogDestination) -> Never {
        log(level: .fatal,
            message: message(),
            synchronous: true,
            fileID: fileID,
            function: function,
            line: line,
            excludeDestination: excludeDestination)
        fatalError(finalMessge(message: message()))
    }

    internal func fatal(string: String,
                        _ fileID: String = #fileID,
                        _ function: String = #function,
                        line: Int = #line,
                        excludeDestination: any LogDestination) -> Never {
        log(level: .fatal,
            message: string,
            synchronous: true,
            fileID: fileID,
            function: function,
            line: line,
            excludeDestination: excludeDestination)
        fatalError(finalMessge(message: string))
    }

    internal func fatal(_ message: @escaping @autoclosure @Sendable () -> String,
                        error: Error,
                        _ fileID: String = #fileID,
                        _ function: String = #function,
                        line: Int = #line,
                        excludeDestination: any LogDestination) -> Never {
        log(level: .fatal,
            message: message(),
            error: error,
            synchronous: true,
            fileID: fileID,
            function: function,
            line: line,
            excludeDestination: excludeDestination)
        fatalError(finalMessge(message: message(), error: error))
    }

    internal func fatal(string: String,
                        error: Error,
                        _ fileID: String = #fileID,
                        _ function: String = #function,
                        line: Int = #line,
                        excludeDestination: any LogDestination) -> Never {
        log(level: .fatal,
            message: string,
            error: error,
            synchronous: true,
            fileID: fileID,
            function: function,
            line: line,
            excludeDestination: excludeDestination)
        fatalError(finalMessge(message: string, error: error))
    }

    internal func fatal(error: Error,
                        _ fileID: String = #fileID,
                        _ function: String = #function,
                        line: Int = #line,
                        excludeDestination: any LogDestination) -> Never {
        log(level: .fatal,
            message: nil,
            error: error,
            synchronous: true,
            fileID: fileID,
            function: function,
            line: line,
            excludeDestination: excludeDestination)
        fatalError(finalMessge(error: error))
    }

    internal func finalMessge(message: @escaping @autoclosure @Sendable () -> String? = nil,
                              error: Error? = nil) -> String {
        let finalMessage = message()
        if let finalMessage,
           let error {
            return "\(finalMessage) \(error.localizedDescription)"
        }
        if let finalMessage {
            return finalMessage
        }
        if let error {
            return error.localizedDescription
        }
        return ""
    }

    internal func expand(fileID: String) -> (moduleName: String, fileName: String) {
        let split = fileID.split(separator: "/")
        guard split.count == 2, // swiftlint:disable:this no_magic_numbers
              let moduleName = split.first,
              let fileName = split.last else {
            return (moduleName: "", fileName: "")
        }
        return (moduleName: String(moduleName), fileName: String(fileName))
    }
}
