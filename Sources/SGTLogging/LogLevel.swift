import Foundation

public enum LogLevel: Int, Sendable {
    private static let maxNameLength = 7

    case verbose = 1
    case debug = 2
    case info = 3
    case warning = 4
    case error = 5
    case fatal = 6

    public init?(emoji: String) {
        switch emoji {
        case "◽️": self = .verbose
        case "◾️": self = .debug
        case "🔷": self = .info
        case "🔶": self = .warning
        case "❌": self = .error
        case "💀": self = .fatal
        default: return nil
        }
    }

    public var name: String {
        switch self {
        case .verbose: return  "VERBOSE"
        case .debug:   return  "DEBUG"
        case .info:    return  "INFO"
        case .warning: return  "WARNING"
        case .error:   return  "ERROR"
        case .fatal:   return  "FATAL"
        }
    }

    public var namePadded: String {
        return name.padding(toLength: Self.maxNameLength, withPad: " ", startingAt: 0)
    }

    public var emoji: String {
        switch self {
        case .verbose: return  "◽️"
        case .debug:   return  "◾️"
        case .info:    return  "🔷"
        case .warning: return  "🔶"
        case .error:   return  "❌"
        case .fatal:   return  "💀"
        }
    }
}
